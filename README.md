# mycode

Test
This is where all my code from the week goes

# mycode (Project Title)

One Paragraph of your project description goes here. Describe what you're trying to do.
What is the purpose of putting up this repo?

## Getting Started

I should not put any public info here!

### Prerequisites

What things are needed to install the software and how to install them. For now, maybe copy in:
"How to Install the Language: "

## Built With

* [Ansible](https://www.ansible.com) - The coding language used
* [Python](https://www.python.org/) - The coding language used

## Authors

* **Your Name** - *Initial work* - [YourWebsite](https://example.com/)
